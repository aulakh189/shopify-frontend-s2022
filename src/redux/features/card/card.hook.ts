// This would normally be in a .env, but since I dont care about the security
// of this application, I left it here for your convenience
const NASA_KEY = 'v7vFwe8h5egeZcZohqgArnfvpyoeAnTRAFcfi2pH';

interface Rover {
  name: string,
}

interface Camera {
  full_name: string,
}

export interface NasaPhoto {
  id: number,
  img_src: string,
  earth_date: string,
  rover: Rover,
  camera: Camera,
  like?: boolean,
}

//I normally like using axios, but to limit dependencies, will use fetch here
export async function fetchNasaImages() {
  const data = await fetch(`https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=${NASA_KEY}`);
  return data.json();
}