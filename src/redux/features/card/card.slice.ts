import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchNasaImages, NasaPhoto } from './card.hook';
import { RootState } from '../../store';

interface NasaResponse {
  photos: NasaPhoto[];
}

export interface CardsState extends NasaResponse {
  status: 'idle' | 'loading' | 'failed';
  likeCount: number;
}

const initialState: CardsState = {
  photos: [],
  status: 'idle',
  likeCount: 0,
};

export const getNasaImagesAsync = createAsyncThunk(
  'cards/fetchNasaImages',
  async () => {
    const response: NasaResponse = await fetchNasaImages();
    return response.photos;
  }
);

export const cardsSlice = createSlice({
  name: 'cards',
  initialState,
  reducers: {
    like: (state, action: PayloadAction<number>) => {
      state.photos[action.payload].like = true;
      state.likeCount += 1;
    },
    unlike: (state, action: PayloadAction<number>) => {
      state.photos[action.payload].like = false;
      if(state.likeCount > 0) state.likeCount -= 1;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getNasaImagesAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getNasaImagesAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.photos = action.payload;
      });
  },
});

export const selectCards = (state: RootState) => state.cards;
export const selectPhoto = (index: number) => (state: RootState) => state.cards.photos[index];

export const { like, unlike } = cardsSlice.actions;
export default cardsSlice.reducer;