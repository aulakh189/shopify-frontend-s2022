import { UIEvent, useState } from 'react';
import './App.css';
import CardsGrid from './components/CardsGrid/CardsGrid';

function App() {
  const [bottom, setBottom] = useState(false);

  /*
    "lazy load". Gives you the idea of how I would implement
    lazyloading, but its not quite accurate as endpoint doesn't 
    support it. But the idea is the same
  */
  const handleScroll = (e: UIEvent<HTMLDivElement>) => {
    const containerHeight = e.currentTarget.clientHeight;
    const scrollHeight = e.currentTarget.scrollHeight;
    const scrollTop = e.currentTarget.scrollTop;
    setBottom(containerHeight === scrollHeight - scrollTop);
	};

  return (
    <div className="App" onScroll={handleScroll}>
      <div className="container">
        <h3>Spacestagram By Sarman Aulakh</h3>
        <h5>Data from Nasa Api</h5>
        <h5>Check out: <a href="https://sarmanaulakh.com/">https://sarmanaulakh.com/</a></h5>
        <CardsGrid bottom={bottom}/>
      </div> 
    </div>
  );
}

export default App;
