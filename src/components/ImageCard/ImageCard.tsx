import styles from './ImageCard.module.css'
import { useAppSelector, useAppDispatch } from '../../redux/hooks';
import {
  like,
  unlike,
  selectPhoto
} from '../../redux/features/card/card.slice';
import Button from '../common/Button/Button';

interface IProps {
  index: number,
}

export default function ImageCard({ index }: IProps) {
  const photo = useAppSelector(selectPhoto(index));
  const dispatch = useAppDispatch();

  const isLiked = (): boolean => photo.like === undefined || photo.like === false ? false : true;

  return (
    <div className={styles.card} style={{backgroundColor: isLiked() ? '#fff5e1' : 'white'}}>
      <img src={photo.img_src} alt="Avatar" />
      <div className={styles.container}>
        <h4><b>{photo.rover.name} rover: {photo.camera.full_name}</b></h4>
        <p>{photo.earth_date}</p>
        <Button width={100} height={45} onClick={() => {isLiked() ? dispatch(unlike(index)) : dispatch(like(index))}}>
          {isLiked() ? "Unlike" : "Like"}
        </Button>
      </div>
    </div>
  )
}
