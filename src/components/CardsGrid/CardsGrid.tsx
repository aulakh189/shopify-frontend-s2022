

import { useState, useEffect } from 'react';
import { useAppSelector, useAppDispatch } from '../../redux/hooks';
import {
  getNasaImagesAsync,
  selectCards
} from '../../redux/features/card/card.slice';
import styles from './CardsGrid.module.css';
import Loader from '../common/Loader/Loader';
import ImageCard from '../ImageCard/ImageCard';
import { NasaPhoto } from '../../redux/features/card/card.hook';

interface IProps {
  bottom: boolean
}

export default function CardsGrid({ bottom }: IProps) {
  const cards = useAppSelector(selectCards);
  const dispatch = useAppDispatch();

  const [cardList, setCardList] = useState(cards.photos);
  const [endIndex, setEndIndex] = useState(10);

  const loading = cards.status === "loading";
  
  useEffect(() => {
    dispatch(getNasaImagesAsync())
      .then(res => {
        const photos: NasaPhoto[] = res.payload as NasaPhoto[];
        setCardList(photos.slice(0, endIndex));
      })
  }, [])

  useEffect(() => {
    if(bottom){
      const newIndex = endIndex + 10;
      setEndIndex(newIndex);
      setCardList(cards.photos.slice(0, newIndex));
    }
  }, [bottom])

  return (
    <div>
      <h5>Liked Photos Counter: {cards.likeCount}</h5>
      <div className={styles.container}>
        {loading && <Loader />}
        {!loading && cardList.map((_, i) => <ImageCard key={i} index={i} />)}
      </div>
    </div>
  )
}
