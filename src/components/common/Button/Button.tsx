import React from 'react'
import styles from './Button.module.css'

interface IProps {
  width?: number,
  height?: number,
  color?: string,
  background?: string,
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void, 
  children?: React.ReactNode,
}

export default function Button({ 
  width = 240, 
  height = 60, 
  color = 'white', 
  background = 'linear-gradient(to right, #FF695A, #FF3C68)',
  onClick,
  children
}: IProps) {
  return (
    <button 
      className={styles.button} 
      onClick={(e) => {
        console.log(e.target)
        onClick(e)
      }}
      style={{background, width, height, color}}
    >
      {children}
    </button>
  )
}
