import styles from './Loader.module.css'

interface IProps {
  size?: number,
  primaryColor?: string,
  secondaryColor?: string
}

export default function Loader({ 
  size = 60, 
  primaryColor = "#f3f3f3", 
  secondaryColor = "#3498db"
}: IProps) {
  return (
    <div className={styles.loader} style={{ 
      width: size, 
      height: size, 
      borderColor: `${primaryColor} ${secondaryColor} ${secondaryColor}`,
    }}>
    </div>
  )
}
